var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');

// css
gulp.task('css', function() {
	gulp.src('./src/scss/main.scss')
		.pipe(sass({
				outputStyle: 'compressed'
			})
			.on('>>> SASS COMPILING ERROR: ', sass.logError))
		.pipe(autoprefixer({
			browsers: ['> 0%'],
			cascade: false
		}))
		.pipe(gulp.dest('./build/css'));
});

// js
gulp.task('index_js', function() {
	gulp.src([
			'src/js/index.js'
		])
		.pipe(concat('index.js'))
		// .pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

// js
gulp.task('word_js', function() {
	gulp.src([
			'src/js/word.js'
		])
		.pipe(concat('word.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

// watch changes for compilation
gulp.task('dev:watch', function () {
	gulp.watch('src/scss/**', ['css']),
	gulp.watch('src/js/index.js', ['index_js']),
	gulp.watch('src/js/word.js', ['word_js']);
});

gulp.task('compile', ['css', 'index_js', 'word_js']);