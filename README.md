# README #

Welcome to the coding assessment for Rafael Contreras

### What is this repository for? ###

* Evaluate Rafael Contreras' coding skills
* Judge Rafael Contreras' design and UX prowess
* Rate Rafael Contreras' overall awesomeness

### How do I get set up? ###

* Clone this repository
* Make sure you have Node installed
* Run `npm i`
* Run `npm start`

On web start go to:

* http://localhost:1234/build/index.html for the Cinema Seat Selector
* http://localhost:1234/build/word.html for the Word inverter

### Who do I talk to? ###

Rafael Contreras

* 021525788
* raf@contrer.as
* https://contrer.as