function invertWords(paragraph){
    return paragraph.split(' ').map(function(word) { 
        var invert = word.split('').reverse().join('');
        // Keep punctuation at the end of word
        if ( ['.',',',':',';','!','?',')',']','}'].indexOf(invert[0]) > -1 ) {
            invert = invert.slice(1) + invert[0];
        }
        // Make first letter capitalized if original word's first letter was capitalized
        if ( /[A-Z]/.test( invert[invert.length - 1] ) ) {
            invert = invert.charAt(0).toUpperCase() + invert.slice(1).toLowerCase();
        }
        return invert;
    }).join(' ');
}

document.getElementById('invertButton').onclick = function() {
    var paragraph = document.getElementById('paragraph').value;
    document.getElementById("result").innerHTML = invertWords(paragraph);
}